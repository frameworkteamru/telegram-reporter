# Exception Collector

Simple decorator for default ExceptionHandler script, which provides ability to track exceptions in your projects.

## Installation

Package is available via composer. `composer require frameworkteamru/telegram-reporter:dev-master`

## Configuration

1. Add `Frameworkteam\TelegramReporter\TelegramReporterServiceProvider::class` to your providers section in `config/app.php`

2. Publish vendor files. `php artisan vendor:publish --provider="Frameworkteam\TelegramReporter\TelegramReporterServiceProvider"`

3. Edit `config/telegram-report.php`

4. Run migrations by `php artisan:migrate`

## Additional Information

1. You can authorize access to routes provided by package
    1. Uncomment `ExceptionsMiddleware::class` in `config/telegram-report.php`.
    2. Implement ExceptionsWatcher interface at least in one of your "User" model.
    3. If you dont like that implementation of authorization, you can put your own middleware into stack in `config/telegram-report.php`

2. Api routes are also provided by this package, so AJAX requests can be handled too

3. To send report in telegram chats, package uses queues, dont forget to set them up