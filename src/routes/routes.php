<?php

Route::group([
    'as' => 'telegram-report.',
    'prefix' => 'telegram-report',
    'namespace' => 'Frameworkteam\TelegramReporter\Controllers',
    'middleware' => config('telegram-report.middleware.common')
], function () {
    require_once 'Api/Api.php';
    require_once 'Web/Web.php';
});
