<?php

Route::group([
    'as' => 'type.',
    'prefix' => 'type',
    'namespace' => 'Type'
], function () {
    require_once 'TypeException.php';

    Route::get('/', ['as' => 'index', 'uses' => 'TypeController@index']);
});
