<?php

Route::group([
    'as' => 'exception.',
    'prefix' => 'exception'
], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'TypeExceptionController@index']);

    Route::group([
        'prefix' => '{exception}'
    ], function () {
        Route::get('/backtrace', ['as' => 'backtrace', 'uses' => 'TypeExceptionController@backtrace']);
        Route::get('/server',    ['as' => 'server',    'uses' => 'TypeExceptionController@server']);
    });
});
