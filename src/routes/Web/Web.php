<?php

Route::group([
    'namespace' => 'Web',
    'middleware' => config('telegram-report.middleware.web')
], function() {
    require_once 'Type/Type.php';
    require_once 'Graph/Graph.php';
});
