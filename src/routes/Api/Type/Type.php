<?php

Route::group([
    'as' => 'type.',
    'prefix' => 'type',
    'namespace' => 'Type'
], function () {
    require_once 'TypeException.php';

    Route::get('/', ['as' => 'index',  'uses' => 'TypeController@index']);

    Route::group([
        'prefix' => '{type}'
    ], function () {
        Route::get('/toggle', ['as' => 'toggle', 'uses' => 'TypeController@toggle']);
        Route::get('/count',  ['as' => 'count',  'uses' => 'TypeController@count']);
    });
});
