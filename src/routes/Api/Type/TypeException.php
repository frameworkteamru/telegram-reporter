<?php

Route::group([
    'as' => 'exception.',
    'prefix' => 'exception'
], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'TypeExceptionController@index']);

    Route::group([
        'prefix' => '{exception}'
    ], function () {
        Route::get('/', ['as' => 'show', 'uses' => 'TypeExceptionController@show']);
    });
});