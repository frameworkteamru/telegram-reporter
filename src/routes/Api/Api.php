<?php

Route::group([
    'as' => 'api.',
    'prefix' => 'api',
    'namespace' => 'Api',
    'middleware' => config('telegram-report.middleware.api')
], function() {
    require_once 'Type/Type.php';
    require_once 'Graph/Graph.php';
});