<?php

Route::group([
    'as' => 'graph.',
    'prefix' => 'graph',
    'namespace' => 'Graph'
], function () {

    Route::get('/',        ['as' => 'index',  'uses' => 'GraphController@index']);
    Route::post('/create', ['as' => 'create', 'uses' => 'GraphController@create']);

    Route::group([
        'prefix' => '{graph}'
    ], function () {
        Route::get('/delete', ['as' => 'delete', 'uses' => 'GraphController@delete']);
    });
});