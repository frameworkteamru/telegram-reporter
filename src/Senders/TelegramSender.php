<?php

namespace Frameworkteam\TelegramReporter\Senders;

use Frameworkteam\TelegramReporter\Interfaces\Sender;

use Frameworkteam\TelegramReporter\Model\Type\TypeException;

use Frameworkteam\TelegramReporter\Jobs\TelegramReportJob;

class TelegramSender implements Sender
{
    private $view;

    public function send(TypeException $exception)
    {
        $this->loadView($exception);
        $this->dispatchJob();
    }

    private function loadView($exception)
    {
        $this->view = view('telegram-report::common/telegram', [
            'message'   => $exception->brief_message,
            'exception' => $exception
        ])->render();
    }

    private function dispatchJob()
    {
        $telegramJob = new TelegramReportJob($this->view);

        $telegramJob->onQueue(config('telegram-report.queue'));

        dispatch($telegramJob);
    }
}
