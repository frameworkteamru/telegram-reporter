<?php

namespace Frameworkteam\TelegramReporter\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramReportJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function handle()
    {
        Telegram::sendMessage([
            'chat_id' => config('telegram-report.chat_id'),
            'text'    => $this->message
        ]);
    }
}
