<?php

namespace Frameworkteam\TelegramReporter\Strategies\Graph;

class YearStrategy extends Strategy
{
    protected $dateFormat = 'Y';
}