<?php

namespace Frameworkteam\TelegramReporter\Strategies\Graph;

class DayStrategy extends Strategy
{
    protected $dateFormat = 'd M, Y';
}