<?php

namespace Frameworkteam\TelegramReporter\Strategies\Graph;

use Frameworkteam\TelegramReporter\Interfaces\GraphStrategy;

use Frameworkteam\TelegramReporter\Factories\GraphQuery\GraphQueryFactory;

abstract class Strategy implements GraphStrategy
{
    protected $factory;

    public function __construct(GraphQueryFactory $factory)
    {
        $this->factory = $factory;
    }

    public function getQuery()
    {
        return $this->factory->create();
    }

    public function getDateFormat()
    {
        return $this->dateFormat;
    }
}