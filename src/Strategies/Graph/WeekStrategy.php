<?php

namespace Frameworkteam\TelegramReporter\Strategies\Graph;

class WeekStrategy extends Strategy
{
    protected $dateFormat = 'W, M, Y';
}