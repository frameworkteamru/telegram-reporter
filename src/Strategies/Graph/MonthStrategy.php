<?php

namespace Frameworkteam\TelegramReporter\Strategies\Graph;

class MonthStrategy extends Strategy
{
    protected $dateFormat = 'M, Y';
}