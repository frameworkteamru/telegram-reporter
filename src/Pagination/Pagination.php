<?php

namespace Frameworkteam\TelegramReporter\Pagination;

use Landish\Pagination\Pagination as BasePagination;

class Pagination extends BasePagination
{
    /**
     * Pagination wrapper HTML.
     *
     * @var string
     */
    protected $paginationWrapper = '<div class="row"><div class="col-md-12 text-md-center"><ul class="pagination">%s %s %s</ul></div></div>';

    /**
     * Available page wrapper HTML.
     *
     * @var string
     */
    protected $availablePageWrapper = '<li class="page-item"><a class="page-link" href="%s">%s</a></li>&nbsp;';

    /**
     * Get active page wrapper HTML.
     *
     * @var string
     */
    protected $activePageWrapper = '<li class="page-item active"><span class="page-link" href="#">%s</span></li>&nbsp;';

    /**
     * Get disabled page wrapper HTML.
     *
     * @var string
     */
    protected $disabledPageWrapper = '<li class="page-item disabled"><a class="page-link" href="#">%s</a></li>&nbsp;';

    /**
     * Previous button text.
     *
     * @var string
     */
    protected $previousButtonText = '<b><</b>';

    /**
     * Next button text.
     *
     * @var string
     */
    protected $nextButtonText = '<b>></b>';

    /***
     * "Dots" text.
     *
     * @var string
     */
    protected $dotsText = '...';
}