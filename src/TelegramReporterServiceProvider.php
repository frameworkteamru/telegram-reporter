<?php

namespace Frameworkteam\TelegramReporter;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Debug\ExceptionHandler;

use Frameworkteam\TelegramReporter\Interfaces\Gate;
use Frameworkteam\TelegramReporter\Gates\ExceptionGate;

use Frameworkteam\TelegramReporter\Interfaces\Sender;
use Frameworkteam\TelegramReporter\Senders\TelegramSender;

use Frameworkteam\TelegramReporter\Model\Type\TypeException;

class TelegramReporterServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->replaceHandler();
        $this->registerSender();
        $this->registerExceptionsGate();
    }

    private function replaceHandler()
    {
        $this->app->singleton(ExceptionHandler::class, TelegramReporter::class);
    }

    private function registerSender()
    {
        $this->app->bind(Sender::class, TelegramSender::class);
    }

    private function registerExceptionsGate()
    {
        $this->app->bind(Gate::class, ExceptionGate::class);
    }

    public function boot()
    {
        $this->bootConfig();
        $this->bootViews();
        $this->bootRoutes();
        $this->bootMigrations();
        $this->bootEloquentEvents();
    }

    private function bootConfig()
    {
        $this->publishes([
            __DIR__ . '/config/telegram-report.php' => config_path('telegram-report.php')
        ], 'config');

        $this->mergeConfigFrom(__DIR__ . '/config/telegram-report.php', 'telegram-report');
    }

    private function bootViews()
    {
        $this->publishes([
            __DIR__ . '/views' => resource_path('views/vendor/telegram-report')
        ], 'views');

        $this->loadViewsFrom(__DIR__ . '/views', 'telegram-report');
    }

    private function bootRoutes()
    {
        require (__DIR__ . '/routes/routes.php');
    }

    private function bootMigrations()
    {
        $this->publishes([
            __DIR__ . '/migrations' => database_path('migrations')
        ], 'migrations');
    }

    private function bootEloquentEvents()
    {
        TypeException::created(function ($exception) {
            $exception->type->exceptionAdded();
        });
    }
}
