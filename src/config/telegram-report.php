<?php

return [
    'enabled' => env('TELEGRAM_REPORT_ENABLED', true),
    'chat_id' => env('TELEGRAM_REPORT_CHAT', '-179194702'),

    'exception_table'      => 'type__exception',
    'exception_type_table' => 'type',
    'graph_table'          => 'graph',

    'queue' => 'default',

    'middleware' => [
        'common' => [
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Illuminate\Auth\Middleware\Authenticate::class,
            // \Frameworkteam\TelegramReporter\Middleware\ExceptionsMiddleware::class,
        ],

        'web' => [
        ],

        'api' => [
        ]
    ]
];