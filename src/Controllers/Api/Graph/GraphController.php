<?php

namespace Frameworkteam\TelegramReporter\Controllers\Api\Graph;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Frameworkteam\TelegramReporter\Model\Graph\Graph;

use Frameworkteam\TelegramReporter\Requests\Graph\GraphCreateRequest;

class GraphController extends Controller
{
    public function index()
    {
        $graphs = Graph::orderBy('id', 'DESC')->paginate(3);

        return response()->json(compact('graphs'));
    }

    public function create(GraphCreateRequest $request)
    {
        $graph = Graph::fromRequest($request);

        return response()->json(compact('graph'));
    }

    public function delete(Graph $graph)
    {
        $graph->forceDelete();

        return response()->json();
    }
}