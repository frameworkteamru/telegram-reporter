<?php

namespace Frameworkteam\TelegramReporter\Controllers\Api\Type;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Frameworkteam\TelegramReporter\Model\Type\TypeException;

class TypeExceptionController extends Controller
{
    public function index(Request $request)
    {
        $filter = collect($request->filter);

        $exceptions = TypeException::filter($filter)->with('type')->orderBy('id', 'DESC')->paginate(10);

        return response()->json(compact('exceptions'));
    }

    public function show(TypeException $exception)
    {
        return response()->json(compact('exception'));
    }
}