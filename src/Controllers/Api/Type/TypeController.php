<?php

namespace Frameworkteam\TelegramReporter\Controllers\Api\Type;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Frameworkteam\TelegramReporter\Model\Type\Type;

class TypeController extends Controller
{
    public function toggle(Type $type)
    {
        $type->toggle();

        return response()->json();
    }

    public function count(Type $type)
    {
        $type->refreshCount();

        $count = $type->total_exceptions_count;

        return response()->json(compact('count'));
    }
}
