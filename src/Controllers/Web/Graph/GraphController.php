<?php

namespace Frameworkteam\TelegramReporter\Controllers\Web\Graph;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Frameworkteam\TelegramReporter\Model\Graph\Graph;

use Frameworkteam\TelegramReporter\Requests\Graph\GraphCreateRequest;

class GraphController extends Controller
{
    public function index(Request $request)
    {
        $filter = collect($request->filter);

        $graphs = Graph::filter($filter)->orderBy('id', 'DESC')->paginate(3);

        return view('telegram-report::graph/index', compact('graphs', 'filter'));
    }

    public function create(GraphCreateRequest $request)
    {
        Graph::fromRequest($request);

        return redirect()->back();
    }

    public function delete(Graph $graph)
    {
        $graph->forceDelete();

        return redirect()->back();
    }
}