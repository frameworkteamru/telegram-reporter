<?php

namespace Frameworkteam\TelegramReporter\Controllers\Web\Type;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Frameworkteam\TelegramReporter\Model\Type\TypeException;

class TypeExceptionController extends Controller
{
    public function index(Request $request)
    {
        $filter = collect($request->filter);

        $exceptions = TypeException::filter($filter)->with('type')->orderBy('id', 'DESC')->paginate(10);

        return view('telegram-report::type/exception/index', compact('exceptions', 'filter'));
    }

    public function backtrace(TypeException $exception)
    {
        return view('telegram-report::type/exception/backtrace', compact('exception'));
    }

    public function server(TypeException $exception)
    {
        return view('telegram-report::type/exception/server', compact('exception'));
    }
}