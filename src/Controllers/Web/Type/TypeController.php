<?php

namespace Frameworkteam\TelegramReporter\Controllers\Web\Type;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Frameworkteam\TelegramReporter\Model\Type\Type;

class TypeController extends Controller
{
    public function index(Request $request)
    {
        $filter = collect($request->filter);

        $types = Type::filter($filter)->orderBy('exception_added_at', 'DESC')->paginate(25);

        return view('telegram-report::type/type/index', compact('types', 'filter'));
    }
}
