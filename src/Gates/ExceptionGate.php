<?php

namespace Frameworkteam\TelegramReporter\Gates;

use Frameworkteam\TelegramReporter\Interfaces\Gate;

use Frameworkteam\TelegramReporter\Model\Type\TypeException;

class ExceptionGate implements Gate
{
    public function denies(TypeException $exception)
    {
        return !$this->allows($exception);
    }

    public function allows(TypeException $exception)
    {
        return $exception->type ? $exception->type->is_enabled : false;
    }
}
