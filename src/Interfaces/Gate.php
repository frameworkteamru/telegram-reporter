<?php

namespace Frameworkteam\TelegramReporter\Interfaces;

use Frameworkteam\TelegramReporter\Model\Type\TypeException;

interface Gate
{
    public function allows(TypeException $exception);

    public function denies(TypeException $exception);
}
