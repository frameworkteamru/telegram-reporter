<?php

namespace Frameworkteam\TelegramReporter\Interfaces;

interface GraphStrategy
{
    public function getQuery();

    public function getDateFormat();
}