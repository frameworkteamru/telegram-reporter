<?php

namespace Frameworkteam\TelegramReporter\Interfaces;

interface ExceptionsWatcher
{
    public function canWatchExceptions();
}