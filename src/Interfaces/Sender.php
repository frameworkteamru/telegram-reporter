<?php

namespace Frameworkteam\TelegramReporter\Interfaces;

use Frameworkteam\TelegramReporter\Model\Type\TypeException;

interface Sender
{
    public function send(TypeException $exception);
}