<?php

namespace Frameworkteam\TelegramReporter;

use App\Exceptions\Handler;

use Frameworkteam\TelegramReporter\Model\Type\TypeException;

use Frameworkteam\TelegramReporter\Interfaces\Gate;
use Frameworkteam\TelegramReporter\Interfaces\Sender;

use Exception;

class TelegramReporter extends Handler
{
    private $handler, $sender, $gate, $exception;

    public function __construct(Handler $handler, Sender $sender, Gate $gate)
    {
        $this->handler = $handler;
        $this->sender = $sender;
        $this->gate = $gate;
    }

    public function report(Exception $originalException)
    {
        $this->handler->report($originalException);

        $this->addException($originalException);

        $this->sendException();
    }

    private function addException($originalException)
    {
        $this->exception = TypeException::add($originalException);
    }

    private function sendException()
    {
        if ($this->gate->allows($this->exception)) {
            $this->sender->send($this->exception);
        }
    }
}
