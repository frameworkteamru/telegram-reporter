<?php

namespace Frameworkteam\TelegramReporter\Factories\GraphQuery;

class YearGraphQueryFactory extends GraphQueryFactory
{
    protected function timeUnitsStatement()
    {
        return "YEAR({$this->exceptionTable}.created_at)";
    }
}