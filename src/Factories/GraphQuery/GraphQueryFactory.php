<?php

namespace Frameworkteam\TelegramReporter\Factories\GraphQuery;

use Illuminate\Support\Facades\DB;

use Frameworkteam\TelegramReporter\Model\Graph\Graph;

abstract class GraphQueryFactory
{
    private $query, $graph;
    protected $exceptionTable, $typeTable;

    abstract protected function timeUnitsStatement();

    final public function __construct(Graph $graph)
    {
        $this->loadConfig();
        $this->makeBasicQuery();
        $this->setGraph($graph);
    }

    final public function create()
    {
        $this->makeQuery();

        return $this->query;
    }

    private function loadConfig()
    {
        $this->exceptionTable = config('telegram-report.exception_table');
        $this->typeTable = config('telegram-report.exception_type_table');
    }

    private function makeBasicQuery()
    {
        $this->query = DB::table($this->exceptionTable);
    }

    private function setGraph($graph)
    {
        $this->graph = $graph;
    }

    private function makeQuery()
    {
        $this->addSelects();
        $this->addJoin();
        $this->addWheres();
        $this->addOrder();
        $this->addGroups();
    }

    private function addSelects()
    {
        $this->addNameAndCount();
        $this->addTimeUnits();
    }

    private function addNameAndCount()
    {
        $this->query->selectRaw("{$this->typeTable}.classname as classname");
        $this->query->selectRaw('count(*) as count');
    }

    private function addTimeUnits()
    {
        $this->query->selectRaw("{$this->timeUnitsStatement()} as date");
    }

    private function addJoin()
    {
        $this->query->join($this->typeTable, "{$this->typeTable}.id", '=', "{$this->exceptionTable}.type_id");
    }

    private function addWheres()
    {
        $this->query->where("{$this->exceptionTable}.created_at", '>', $this->graph->start_date);
        $this->query->where("{$this->exceptionTable}.created_at", '<', $this->graph->end_date);
    }

    private function addOrder()
    {
        $this->query->orderByRaw('LENGTH(date), date', 'ASC');
    }

    private function addGroups()
    {
        $this->query->groupBy('classname', 'date');
    }
}
