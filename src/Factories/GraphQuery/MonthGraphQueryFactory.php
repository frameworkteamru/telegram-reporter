<?php

namespace Frameworkteam\TelegramReporter\Factories\GraphQuery;

class MonthGraphQueryFactory extends GraphQueryFactory
{
    protected function timeUnitsStatement()
    {
        return "CONCAT_WS(\"-\", YEAR({$this->exceptionTable}.created_at), MONTH({$this->exceptionTable}.created_at))";
    }
}