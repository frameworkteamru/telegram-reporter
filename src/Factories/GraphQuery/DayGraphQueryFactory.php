<?php

namespace Frameworkteam\TelegramReporter\Factories\GraphQuery;

class DayGraphQueryFactory extends GraphQueryFactory
{
    protected function timeUnitsStatement()
    {
        $createdField = "{$this->exceptionTable}.created_at";

        return "CONCAT_WS(\"-\", YEAR({$createdField}), MONTH({$createdField}), DAY({$createdField}))";
    }
}