<?php

namespace Frameworkteam\TelegramReporter\Factories\GraphQuery;

class WeekGraphQueryFactory extends GraphQueryFactory
{
    protected function timeUnitsStatement()
    {
        return "CONCAT(YEAR({$this->exceptionTable}.created_at), 'W', LPAD(WEEK({$this->exceptionTable}.created_at), 2, '0'))";
    }
}