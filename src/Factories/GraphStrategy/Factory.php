<?php

namespace Frameworkteam\TelegramReporter\Factories\GraphStrategy;

use Frameworkteam\TelegramReporter\Model\Graph\Graph;

use Frameworkteam\TelegramReporter\Strategies;
use Frameworkteam\TelegramReporter\Factories\GraphQuery;

use RuntimeException;

class Factory
{
    public static function create(Graph $graph)
    {
        switch ($graph->graph_units) {
            case Graph::DAY:
                return new Strategies\Graph\DayStrategy(new GraphQuery\DayGraphQueryFactory($graph));
            case Graph::WEEK:
                return new Strategies\Graph\WeekStrategy(new GraphQuery\WeekGraphQueryFactory($graph));
            case Graph::MONTH:
                return new Strategies\Graph\MonthStrategy(new GraphQuery\MonthGraphQueryFactory($graph));
            case Graph::YEAR:
                return new Strategies\Graph\YearStrategy(new GraphQuery\YearGraphQueryFactory($graph));
            default:
                throw new RuntimeException;
        }
    }
}
