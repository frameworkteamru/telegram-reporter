<?php

namespace Frameworkteam\TelegramReporter\Model\Graph;

use Frameworkteam\TelegramReporter\Model\Base\Graph\Graph as BaseGraph;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Carbon\Carbon;

class Graph extends BaseGraph
{
    const DAY = 1;
    const WEEK = 2;
    const MONTH = 3;
    const YEAR = 4;

    const LABELS = [
        self::DAY => 'Day',
        self::WEEK => 'Week',
        self::MONTH => 'Month',
        self::YEAR => 'Year'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'start_date',
        'end_date'
    ];

    protected $appends = [
        'data'
    ];

    public static function fromRequest(Request $request)
    {
        return static::create([
            'name'        => $request->name,
            'graph_units' => $request->graph_units,
            'start_date'  => Carbon::parse($request->start_date),
            'end_date'    => Carbon::parse($request->end_date)
        ]);
    }

    public function scopeFilter($query, $filter)
    {
        if (!empty($filter->get('name')))
        {
            $query->whereRaw("name REGEXP '{$filter->get('name')}'");
        }

        return $query;
    }

    public function getDateFormatAttribute()
    {
        $this->setStrategyIfNotSet();

        return $this->strategy->getDateFormat();
    }

    public function getDataAttribute()
    {
        $this->setStrategyIfNotSet();

        return $this->strategy->getQuery()->get();
    }
}
