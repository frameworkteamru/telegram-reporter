<?php

namespace Frameworkteam\TelegramReporter\Model\Base\Graph;

use Illuminate\Database\Eloquent\Model;

use Frameworkteam\TelegramReporter\Interfaces\GraphStrategy;

use Frameworkteam\TelegramReporter\Factories\GraphStrategy\Factory;

class Graph extends Model
{
    public $timestamps = true;

    protected $table, $strategy;

    protected $fillable = [
        'name',
        'graph_units',
        'start_date',
        'end_date'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('telegram-report.graph_table'));
    }

    protected function setStrategyIfNotSet()
    {
        if (is_null($this->strategy)) {
            $this->strategy = Factory::create($this);
        }
    }
}