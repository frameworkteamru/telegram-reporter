<?php

namespace Frameworkteam\TelegramReporter\Model\Base\Type;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public $timestamps = true;

    protected $table;

    protected $fillable = [
        'classname',
        'is_enabled',
        'exception_added_at',
        'total_exceptions_count'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('telegram-report.exception_type_table'));
    }

    public function exception()
    {
        return $this->hasMany('Frameworkteam\TelegramReporter\Model\Type\TypeException', 'type_id', 'id');
    }
}