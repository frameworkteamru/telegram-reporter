<?php

namespace Frameworkteam\TelegramReporter\Model\Base\Type;

use Illuminate\Database\Eloquent\Model;

class TypeException extends Model
{
    public $timestamps = true;

    protected $table;

    protected $fillable = [
        'stacktrace',
        'server_env',
        'type_id'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('telegram-report.exception_table'));
    }

    public function type()
    {
        return $this->belongsTo('Frameworkteam\TelegramReporter\Model\Type\Type', 'type_id', 'id');
    }
}