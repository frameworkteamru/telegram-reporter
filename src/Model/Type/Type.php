<?php

namespace Frameworkteam\TelegramReporter\Model\Type;

use Frameworkteam\TelegramReporter\Model\Base\Type\Type as BaseType;

use Carbon\Carbon;

use Exception;

class Type extends BaseType
{
    public static function fromException(Exception $exception)
    {
        return static::firstOrCreate([
            'classname' => get_class($exception)
        ]);
    }

    public function scopeFilter($query, $filter)
    {
        if (!is_null($filter->get('is_enabled')))
        {
            $query->whereIsEnabled($filter->get('is_enabled'));
        }

        if ($filter->get('classname'))
        {
            $query->whereRaw('classname REGEXP ?', [$filter->get('classname')]);
        }

        return $query;
    }

    public function toggle()
    {
        $this->update([
            'is_enabled' => !$this->is_enabled
        ]);
    }

    public function exceptionAdded()
    {
        $this->update([
            'exception_added_at' => Carbon::now(),
            'total_exceptions_count' => ++$this->total_exceptions_count
        ]);
    }

    public function refreshCount()
    {
        $this->update([
            'total_exceptions_count' => $this->exception()->count()
        ]);
    }
}
