<?php

namespace Frameworkteam\TelegramReporter\Model\Type;

use Frameworkteam\TelegramReporter\Model\Base\Type\TypeException as BaseTypeException;

use Carbon\Carbon;
use Exception;
use Request;

class TypeException extends BaseTypeException
{
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function scopeFilter($query, $filter)
    {
        $typeTable = config('telegram-report.exception_type_table');

        if (!empty($filter->get('type')))
        {
            $escapedType = str_replace('\\', '\\\\\\\\', $filter->get('type'));

            $query->select("{$this->table}.*")
                ->join($typeTable, "{$typeTable}.id", "{$this->table}.type_id")
            ->whereRaw("{$typeTable}.classname REGEXP '{$escapedType}'");
        }

        if (!empty($filter->get('start_date')))
        {
            $query->where('created_at', '>', Carbon::parse($filter->get('start_date')));
        }

        if (!empty($filter->get('end_date')))
        {
            $query->where('created_at', '<', Carbon::parse($filter->get('end_date')));
        }

        return $query;
    }

    public static function add(Exception $exception)
    {
        $type = Type::fromException($exception);

        return static::create([
            'stacktrace' => $exception->__toString(),
            'server_env' => var_export(Request::server(), true),
            'type_id'    => $type->id
        ]);
    }

    public function getBriefMessageAttribute()
    {
        return array_first(explode("\n", $this->stacktrace));
    }
}