<?php

namespace Frameworkteam\TelegramReporter\Middleware;

use Frameworkteam\TelegramReporter\Interfaces\ExceptionsWatcher;

use Illuminate\Http\Request;

use Symfony\Component\HttpKernel\Exception\HttpException;

use Closure;
use Auth;

class ExceptionsMiddleware
{
    private $users = [];

    public function __construct()
    {
        $this->loadAuthenticated();
        $this->loadImplemented();
    }

    private function loadAuthenticated()
    {
        foreach (array_keys(config('auth.guards')) as $guard) {
            if (Auth::guard($guard)->check()) {
                $this->users[] = Auth::guard($guard)->user();
            }
        }
    }

    private function loadImplemented()
    {
        $this->users = array_filter($this->users, function ($user) {
            return ($user instanceof ExceptionsWatcher);
        });
    }

    public function handle(Request $request, Closure $next)
    {
        if (!$this->authorize()) {
            abort(403);
        }

        return $next($request);
    }

    private function authorize()
    {
        return array_reduce($this->users, function ($allows, $user) {
            return ($allows || $user->canWatchExceptions());
        }, false);
    }
}
