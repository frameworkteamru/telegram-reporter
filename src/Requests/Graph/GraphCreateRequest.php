<?php

namespace Frameworkteam\TelegramReporter\Requests\Graph;

use Illuminate\Foundation\Http\FormRequest;

class GraphCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'        => 'required',
            'start_date'  => 'required|date',
            'end_date'    => 'required|date',
            'graph_units' => 'required|numeric',
        ];
    }
}
