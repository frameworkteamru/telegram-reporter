@php
    $i = 0;
@endphp

@foreach(array_keys(config('auth.guards')) as $guard)
@if (Auth::guard($guard)->check())
@php
    $i++;
@endphp
{{ ucfirst($guard) }}: {{ get_class(Auth::guard($guard)->user()) }} #{{ Auth::guard($guard)->id() }}
@endif
@endforeach

@if (!$i)
Guest
@endif

Charts: {{ route('telegram-report.graph.index') }}

Server Environment: {{ route('telegram-report.type.exception.server', $exception->id) }}
Backtrace: {{ route('telegram-report.type.exception.backtrace', $exception->id) }}

Url: {{ Request::url() }}
Method: {{ Request::method() }}
Action: {{ Route::currentRouteAction() }}

Exception: {{ $message }}
