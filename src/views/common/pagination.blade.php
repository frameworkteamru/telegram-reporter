@if (str_is('5.2*', app()->version()))
{!! $paginator->appends($appends)->links((new \Frameworkteam\TelegramReporter\Pagination\Pagination($paginator))) !!}
@else
{!! $paginator->appends($appends)->links('telegram-report::pagination/pagination') !!}
@endif