@php
    $data = collect($graph->data);
    $classes = $data->pluck('classname')->unique();
@endphp

current = charts.length;

charts[current] = {id: {{ $graph->id }}};
charts[current].data = [
    ['Date',
        @forelse ($classes as $class)
            '{!! preg_replace('/.*?\\\?(\w+)$/', '\1', $class) !!}',
        @empty
            'No exceptions yet'
        @endforelse
    ],
    @forelse ($data->groupBy('date') as $date => $row)
        ['{{ \Carbon\Carbon::parse($date)->format($graph->date_format) }}',
            @foreach ($classes as $class)
                @if ($stats = $row->where('classname', $class)->first())
                    {{ $stats->count }},
                @else
                    0,
                @endif
            @endforeach
        ],
    @empty
        ['{{ \Carbon\Carbon::now()->format('d M, Y') }}', 0]
    @endforelse
];