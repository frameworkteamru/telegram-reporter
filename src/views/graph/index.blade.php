@extends('telegram-report::layouts/filter')

@section('filter')
    @include('telegram-report::graph/filter')
@endsection

@section('main-content')
    @include('telegram-report::graph/index-scripts')

    @forelse ($graphs as $graph)
        @include('telegram-report::graph/card')
    @empty
        <div class="alert alert-info">
            @if ($filter->isEmpty())
                There are no graphs yet.
            @else
                No search results
            @endif
        </div>
    @endforelse

    @include('telegram-report::common/pagination', [
        'appends'   => request()->input(),
        'paginator' => $graphs
    ])
@endsection