<form class="form" action="{{ route('telegram-report.graph.create') }}" method="POST" id="create-form">
    {{ csrf_field() }}
    <div class="form-group @if ($nameErrors = $errors->get('name')) has-danger @endif">
        <label for="inputName">Name</label>
        <input type="text"
               id="inputName"
               class="form-control"
               aria-describedby="nameHelpInline"
               name="name"
               value="{{ old('name') }}">
        @if ($nameErrors)
            <div class="form-control-feedback">
                @foreach ($nameErrors as $error)
                    {{ $error }}
                @endforeach
            </div>
        @endif
        <small id="nameHelpInline" class="text-muted">
            Required. Name of the graph
        </small>
    </div>

    <div class="form-group @if ($dateErrors = $errors->get('start_date')) has-danger @endif">
        <label for="inputStartDate">Start date</label>
        <input type="text"
               id="inputStartDate"
               class="form-control"
               aria-describedby="startDateHelpInline"
               name="start_date"
               value="{{ old('start_date') }}">
        @if ($dateErrors)
            <div class="form-control-feedback">
                @foreach ($dateErrors as $error)
                    {{ $error }}
                @endforeach
            </div>
        @endif
        <small id="startDateHelpInline" class="text-muted">
            Start date to track exceptions
        </small>
    </div>

    <div class="form-group @if ($dateErrors = $errors->get('end_date')) has-danger @endif">
        <label for="inputEndDate">End date</label>
        <input type="text"
               id="inputEndDate"
               class="form-control"
               aria-describedby="endDateHelpInline"
               name="end_date"
               value="{{ old('end_date') }}">
        @if ($dateErrors)
            <div class="form-control-feedback">
                @foreach ($dateErrors as $error)
                    {{ $error }}
                @endforeach
            </div>
        @endif
        <small id="endDateHelpInline" class="text-muted">
            End date to track exceptions
        </small>
    </div>

    <div class="form-group @if ($unitErrors = $errors->get('graph_units')) has-danger @endif">
        <label for="graph_units">Precision</label>
        <select id="graph_units" class="form-control" aria-describedby="graph_units" name="graph_units">
            <option disabled="true">Please, select an option</option>
            @foreach (\Frameworkteam\TelegramReporter\Model\Graph\Graph::LABELS as $unit => $label)
                <option value="{{ $unit }}" @if ($unit == old('name')) selected @endif>{{ $label }}</option>
            @endforeach
        </select>
        @if ($unitErrors)
            <div class="form-control-feedback">
                @foreach ($unitErrors as $error)
                    {{ $error }}
                @endforeach
            </div>
        @endif
        <small id="graph_units" class="text-muted">
            How precisely results will be displayed
        </small>
    </div>
</form>

<script type="text/javascript">
    @if (!$errors->isEmpty())
        $('#create-modal').modal('show')
    @endif

    $(function () {
        $('#inputStartDate').datetimepicker({
            format: 'DD-MM-Y HH:mm:ss',
        });
        $('#inputEndDate').datetimepicker({
            format: 'DD-MM-Y HH:mm:ss',
            useCurrent: false
        });
        $("#inputStartDate").on("dp.change", function (e) {
            $('#inputEndDate').data("DateTimePicker").minDate(e.date);
        });
        $("#inputEndDate").on("dp.change", function (e) {
            $('#inputStartDate').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>