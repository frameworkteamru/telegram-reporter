<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    var current;
    var charts = [];
    var callbacks = [];
    var chartOptions = {
        legend: {position: 'right'},
        hAxis: {titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
    };

    @foreach ($graphs as $graph)
        @include('telegram-report::graph/index-script')
    @endforeach

    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(function() {
        makeChartCallbacks();
        runCallbacks();
        registerResizeEvent();
    });

    var makeChartCallbacks = function() {
        for (var chart in charts) {
            makeChartCallback(charts[chart].data, `#${charts[chart].id}`);
        }
    };

    var runCallbacks = function() {
        for (var i in callbacks) {
            callbacks[i]();
        }
    };

    var registerResizeEvent = function() {
        $(window).resize(function() {
            runCallbacks();
        });
    };

    var makeChartCallback = function(source, selector) {
        var chart = new google.visualization.ComboChart($(selector)[0]);
        var data = google.visualization.arrayToDataTable(source);

        google.visualization.events.addListener(chart, 'select', function() {
            toggleColumn(chart, data);
        });

        callbacks[callbacks.length] = function() {
            redrawKeepingToggled(chart, data);
        };
    };

    var toggleColumn = function(chart, data, keepToggled = false) {
        var column = chart.getSelection()[0].column;
        var view = new google.visualization.DataView(data);
        chart.toggled = !chart.toggled;

        if (chart.toggled || keepToggled) {
            view.setColumns([0, column]);
            chart.draw(view, chartOptions);
        } else {
            chart.draw(data, chartOptions);
        }
    };

    var redrawKeepingToggled = function(chart, data) {
        if (!chart.toggled) {
            chart.draw(data, chartOptions);
        } else {
            toggleColumn(chart, data, true);
        }
    };
</script>