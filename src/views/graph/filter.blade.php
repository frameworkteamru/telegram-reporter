@include('telegram-report::graph/create-modal')

<div class="row">
    <div class="col-md-5">
        <form class="form-inline" id="graph-filter">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text"
                       class="form-control"
                       id="name"
                       placeholder="Graph Name"
                       name="filter[name]"
                       value="{{ $filter->get('name') }}">
            </div>
        </form>
    </div>
    <div class="col-md-4 offset-md-3 text-md-right">
        <div class="btn-group">
            <button type="button" class="btn btn-primary" onclick="$('#graph-filter').submit()">Find</button>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-modal">
                Create New Graph
            </button>
        </div>
    </div>
</div>
