<div class="modal fade" id="delete-modal-{{ $graph->id }}" tabindex="-1" role="dialog" aria-labelledby="delete-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="delete-modal-label">Delete Graph `{{ $graph->name }}`?</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Data presented in this graph will be kept.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                <a href="{{ route('telegram-report.graph.delete', $graph->id) }}" class="btn btn-danger">
                   Delete
                </a>
            </div>
        </div>
    </div>
</div>