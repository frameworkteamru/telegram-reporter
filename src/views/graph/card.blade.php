@include('telegram-report::graph/delete-modal')

<div class="card card-block card-outline-secondary">
    <div class="card card-block card-outline-secondary">
        <div class="col-md-4">
            <h4>{{ $graph->name }}</h4>
        </div>
        <div class="col-md-2 offset-md-6 text-md-right">
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-modal-{{ $graph->id }}">
                Delete
            </button>
        </div>
    </div>
    <div class="card card-block card-outline-secondary">
        <div id="{{ $graph->id }}"
             style="width: 100%; min-height: 600px;">
        </div>
    </div>
</div>