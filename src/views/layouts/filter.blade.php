@extends('telegram-report::layouts.left-menu')

@section('right-content')
    <div class="card card-block">
        @yield('filter')
    </div>
    <div class="row">
        <div class="col-md-12">
            @yield('main-content')
        </div>
    </div>
@endsection