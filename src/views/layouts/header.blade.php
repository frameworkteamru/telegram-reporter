@extends('telegram-report::layouts.base')

@section('content')
    <nav class="navbar navbar-dark bg-primary">
        <h1 class="navbar-brand mb-0">{{ config('app.name') }}@exceptions</h1>
    </nav>

    <br>

    <div class="row">
        <div class="col-md-12">
            @yield('middle-content')
        </div>
    </div>
@endsection