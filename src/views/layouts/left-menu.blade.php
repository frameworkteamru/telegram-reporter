@extends('telegram-report::layouts.header')

@section('middle-content')
    <div class="row">
        <div class="col-md-2">
            @section('left-menu')
                <nav class="nav nav-pills nav-stacked">
                    <a class="nav-link {{ Route::is('telegram-report.graph.index') ? 'active' : '' }}"
                       href="{{ route('telegram-report.graph.index') }}">
                        Graphs
                    </a>
                    <a class="nav-link {{ Request::is('*telegram-report/type') ? 'active' : '' }}"
                       href="{{ route('telegram-report.type.index') }}">
                        Types
                    </a>
                    <a class="nav-link {{ Request::is('*telegram-report/type/exception*') ? 'active' : '' }}"
                       href="{{ route('telegram-report.type.exception.index') }}">
                        Exceptions
                    </a>
                    <a class="nav-link" href="#" onclick="toastr.error('No one can help you')">Help</a>
                </nav>
            @show
        </div>
        <div class="col-md-10">
            @yield('right-content')
        </div>
    </div>
@endsection