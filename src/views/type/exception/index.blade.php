@extends('telegram-report::layouts/filter')

@section('filter')
    @include('telegram-report::type/exception/filter')
@endsection

@section('main-content')
    @forelse($exceptions as $exception)
        @include('telegram-report::type/exception/card')
    @empty
        <div class="alert alert-info">
            @if ($filter->isEmpty())
                There are no exceptions.
            @else
                No search results.
            @endif
        </div>
    @endforelse

    @include('telegram-report::common/pagination', [
        'appends'   => request()->input(),
        'paginator' => $exceptions
    ])
@endsection