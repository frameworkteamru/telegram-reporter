<form class="form" action="{{ route('telegram-report.type.exception.index') }}" method="GET">
    <div class="form-group row">
        <label for="exception_type" class="col-xs-2 col-form-label">
            Type:
        </label>
        <div class="col-xs-10">
            <input type="text"
                   class="form-control"
                   id="exception_type"
                   placeholder="NotFoundHttpException"
                   name="filter[type]"
                   value="{{ $filter->get('type') }}">
        </div>
    </div>
    <div class="form-group row">
        <label for="start_date" class="col-xs-2 col-form-label">
            Start Date:
        </label>
        <div class="col-xs-10">
            <input type="text"
                   class="form-control"
                   id="start_date"
                   placeholder="Start Date"
                   name="filter[start_date]"
                   value="{{ $filter->get('start_date') }}">
        </div>
    </div>
    <div class="form-group row">
        <label for="end_date" class="col-xs-2 col-form-label">
            End Date:
        </label>
        <div class="col-xs-10">
            <input type="text"
                   class="form-control"
                   id="end_date"
                   placeholder="End Date"
                   name="filter[end_date]"
                   value="{{ $filter->get('end_date') }}">
        </div>
    </div>
    <div class="form-group row">
        <div class="btn-group col-xs-4 offset-xs-8" role="group">
            <button type="button" class="col-xs-6 btn btn-secondary" id="reset">Reset</button>
            <button type="submit" class="col-xs-6 btn btn-primary">Find</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(function () {
        $('#start_date').datetimepicker({
            format: 'DD-MM-Y HH:mm:ss'
        });
        $('#end_date').datetimepicker({
            format: 'DD-MM-Y HH:mm:ss',
            useCurrent: false
        });
        $("#start_date").on("dp.change", function (e) {
            $('#end_date').data("DateTimePicker").minDate(e.date);
        });
        $("#end_date").on("dp.change", function (e) {
            $('#start_date').data("DateTimePicker").maxDate(e.date);
        });
        $("#reset").click(function() {
            $(this).closest('form').find("input[type=text], textarea").val("");
        });
    });
</script>