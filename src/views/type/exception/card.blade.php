<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-3">
                Exception #{{ $exception->id }}
            </div>
            <div class="col-md-4 offset-md-5 text-md-right">
                <a href="{{ route('telegram-report.type.exception.server', $exception->id) }}"
                   target="_blank"
                   class="btn btn-sm btn-default">
                    Raw Server ENV
                </a>
                <a href="{{ route('telegram-report.type.exception.backtrace', $exception->id) }}"
                   target="_blank"
                   class="btn btn-sm btn-default">
                    Raw Backtrace
                </a>
            </div>
        </div>
    </div>
    <div class="card-block">
        <p class="card-text">
            Date: <small>{{ $exception->created_at->format('d, M Y H:i:s') }}</small>
        </p>
        <p class="card-text">
            Type: <small>{{ $exception->type->classname }}</small>
        </p>
        <p class="card-text">
            Brief Message:
            <small>{{ $exception->brief_message }}</small>
        </p>
    </div>
</div>