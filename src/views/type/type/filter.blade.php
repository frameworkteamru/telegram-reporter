<form class="form" action="{{ route('telegram-report.type.index') }}" method="GET">
    <div class="form-group row">
        <label for="exception_type" class="col-xs-2 col-form-label">
            Classname:
        </label>
        <div class="col-xs-5">
            <input type="text"
                   class="form-control"
                   placeholder="NotFoundHttpException"
                   name="filter[classname]"
                   value="{{ $filter->get('classname') }}">
        </div>
        <div class="col-xs-3">
            <label class="form-check-label col-form-label">
                <input type="checkbox"
                       class="form-check-input"
                       name="filter[is_enabled]"
                       value="1"
                       @if ($filter->get('is_enabled')) checked @endif>
                Reporting Enabled
            </label>
        </div>
        <div class="col-xs-2">
          <button class="btn btn-primary btn-block">Find</button>
        </div>
    </div>
</form>