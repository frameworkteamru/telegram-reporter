<script type="text/javascript">
    (function () {
        $('.js-count-button').click(function () {
            refreshCount($(this).data('id'));
        });

        var refreshCount = function (id) {
            $.get(`/telegram-report/api/type/${id}/count`, function (response) {
                updateView(id, response.count);
            });
        };

        var updateView = function (id, count) {
            $(`.js-count-container[data-id=${id}]`).text(count);
        };
    })();
</script>
