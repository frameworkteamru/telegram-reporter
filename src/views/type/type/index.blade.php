@extends('telegram-report::layouts/filter')

@section('filter')
    @include('telegram-report::type/type/filter')
@endsection

@section('main-content')
    @forelse($types as $type)
        @include('telegram-report::type/type/card')
    @empty
        <div class="alert alert-info">
            @if ($filter->isEmpty())
                There are no types.
            @else
                No search results.
            @endif
        </div>
    @endforelse

    @include('telegram-report::common/pagination', [
        'appends'   => request()->input(),
        'paginator' => $types
    ])

    @include('telegram-report::/type/type/toggle-script')
    @include('telegram-report::/type/type/count-script')
@endsection