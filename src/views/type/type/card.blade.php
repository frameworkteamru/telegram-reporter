<div class="card">
    <div class="card-block">
        <div class="row">
            <div class="col-md-3">
                <small>
                    <a href="{{ route('telegram-report.type.exception.index', [
                        'filter' => ['type' => $type->classname]
                    ]) }}">
                        {{ $type->classname }}
                    </a>
                </small>
                <br>
                <small>
                    Exceptions count:
                    <span class="js-count-container" data-id="{{ $type->id }}">
                        {{ $type->total_exceptions_count }}
                    </span>
                </small>
            </div>
            <div class="col-md-4 offset-md-5 text-md-right">
                <button class="btn btn-warning js-count-button"
                        data-id="{{ $type->id }}">
                    Refresh Counter
                </button>
                @if ($type->is_enabled)
                    <button type="button"
                            href="javascript:void(0);"
                            class="btn btn-danger js-toggle-button"
                            data-id="{{ $type->id }}"
                            data-enabled="true">
                        Disable
                    </button>
                @else
                    <button type="button"
                            class="btn btn-success js-toggle-button"
                            data-id="{{ $type->id }}"
                            data-enabled="false">
                        Enable
                    </button>
                @endif
            </div>
        </div>
    </div>
</div>