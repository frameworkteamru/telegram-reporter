<script type="text/javascript">
    (function () {
        $('.js-toggle-button').click(function () {
            toggle($(this).data('id'));
        });

        var toggle = function (id) {
            $.get(`/telegram-report/api/type/${id}/toggle`, function () {
                toggleButton(id);
            });
        };

        var toggleButton = function (id) {
            var button = $(`.js-toggle-button[data-id=${id}]`);

            button.data('enabled', !button.data('enabled'));

            toggleTemplate(button);
        };

        var toggleTemplate = function (button) {
            if (button.data('enabled')) {
                showDisableButton(button);
            } else {
                showEnableButton(button);
            }
        };

        var showDisableButton = function (button) {
            button.removeClass('btn-success');
            button.addClass('btn-danger');
            button.text('Disable');
        };

        var showEnableButton = function (button) {
            button.removeClass('btn-danger');
            button.addClass('btn-success');
            button.text('Enable');
        };
    })();
</script>