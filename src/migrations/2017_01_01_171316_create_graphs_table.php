<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraphsTable extends Migration
{
    public function up()
    {
        Schema::create(config('telegram-report.graph_table'), function (Blueprint $table) {
            $table->increments('id');

            $table->integer('graph_units');
            $table->string('name');
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();

            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop(config('telegram-report.graph_table'));
    }
}
