<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExceptionsTable extends Migration
{
    public function up()
    {
        Schema::create(config('telegram-report.exception_table'), function (Blueprint $table) {
            $table->increments('id');

            $table->integer('type_id')->unsigned();
            $table->text('stacktrace');
            $table->text('server_env');

            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();

            $table->foreign('type_id')->references('id')
                ->on(config('telegram-report.exception_type_table'))
            ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop(config('telegram-report.exception_table'));
    }
}
