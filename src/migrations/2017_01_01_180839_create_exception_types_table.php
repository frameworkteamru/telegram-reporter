<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExceptionTypesTable extends Migration
{
    public function up()
    {
        Schema::create(config('telegram-report.exception_type_table'), function (Blueprint $table) {
            $table->increments('id');

            $table->text('classname');
            $table->integer('total_exceptions_count')->default(0);
            $table->boolean('is_enabled')->default(true);

            $table->datetime('exception_added_at')->nullable();
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop(config('telegram-report.exception_type_table'));
    }
}
